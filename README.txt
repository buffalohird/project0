Ansel Duff
Buffalo Hird
Project0
Computer Science 164

README.txt - Instructions on constructing our mobile web application.

Design Choices:

Views-
    We decided to utilize our version of local storage which cuts off the shopping list after 100 items. This seemed
    wholly sufficient considering how one can only take 4 courses.  Instead of having to iterate through the ID field
    to find a way to clear removed courses (this is likewise true for the enrolled list), we simply reset the counter
    when the user clicks on the search option from an empty shopping list.  In real life usage, although not ideal, 
    the user will click on this before having had 100 courses in their shopping list 99% of the time.  This was a 
    matter of performance and code complexity
    
    We utilize similar code for shopping and enrolled courses without factoring out any information, simply because of
    the mere singular repetition and this factoring out would require further variables to pick the proper variable for
    each field.
    
    We utilized 'enter' as a send key, because this seems more mobile.  It did, however, require that slightly oddly
    formatted event for the sidebar.
    
    We kept our scripts in the views just out of simplicity's sake, which seemed reasonable for such a small project
    
    We placed enrolled courses on the welcome page due to a simplification of our original ambitions, leaving a slightly
    odd organization, but which is more logical with the combination of searching and recents in the sidebar.
    
    We used $(document).ready because other events would never load css.  This is our most major issue in that the site
    does not always properly load css.  In fact, it also fails to hide the default empty shopping list text when the css
    does load.  For version 1 we are not too upset about it, as functionality is 100%.  However, it is an annoyance and 
    we made the decision to prioritze functionality while working on the site, running out of time to fix the css.
    
    We placed our search and recent info on separate view files, where they could be modified dynamically with php and
    then returned to the page proper.  This method seems to work well and requires loading much less data than an html
    request.  It also factors out what could be messy php code if included in the default index.
    
    We had to pull our livetile idea since we had to reprioritize, by our live rob tiles might be even better than that
    idea could ever aspire to be.

Models-
	Our models work to optimize the user's searching/retrieving capabilities. They work by joining three relational tables 
	contrained in our jharvard_project0 database. The biggest, most substantial of the two is the sidebar model which tries
	to match the user's search input string as best it can to any information about a course. It has some tring manipulation
	functions that looks for names, as well as matching input keys with a specified gen ed, time, or day configuration.
	
	The sidebar-model has a bunch of code commented out that we couldn't get to work properyl. Again, we're new to this
	MVC framwork and really had a lot of trouble with this project.
	
	
Controllers-  
    Our controllers are pretty straight-forward. They obveously allow our models and views to communicate. We have has some issues
    reloading certain views within a page, as we are VERY new to CodeIgniter and are somewhat confused by a lot of the hand-waiving
    that it does and we don't understand. 
_________________________________

# clone repo into ~/vhosts/buffalohird
cd ~/vhosts
git clone git@bitbucket.org:buffalohird/project0.git buffalohird

# chmod all directories 711
find ~/vhosts/buffalohird -type d -exec chmod 711 {} \;

# chmod all PHP files 600
find ~/vhosts/buffalohird -type f -name *.php -exec chmod 600 {} \;

# chmod most everything else 644
find ~/vhosts/buffalohird -type f \( -name *.css -o -name *.gif -o -name *.html -o -name *.js -o -name *.jpg -o -name *.png -o -name .htaccess \) -exec chmod 644 {} \;

# create a MySQL database for project
mysql -u jharvard -p -e 'CREATE DATABASE jharvard_buffalohird'

# import SQL dump into database
mysql -u jharvard -p jharvard_buffalohird < ~/vhosts/buffalohird/jharvard_project0.sql

# change the value of $db['default']['database'] to be 'jharvard_buffalohird'
gedit ~/vhosts/buffalohird/application/config/database.php

# append '127.0.0.1 buffalohird' to /etc/hosts
sudo gedit /etc/hosts
