<!-- *********************************************************
 *
 * welcome/index.php
 * 
 * View for CS164 Courses Homepage
 *
 * Buffalo Hird
 * Computer Science 164
 * Project0
 *
 *
 ********************************************************* -->
    <script>
        $(document).ready(function() {
    
            $(".rob_tile").attr("src","http://www.cs50.net/eyes.png");

        
            //sets the enrolled course information based on localStorage info
            
            //loads number of courses from storage so that each can be added to shopping list
            var lastTakeID = localStorage.getItem('lastTakeID');
            //list starts at 0
            TakeID = 100;
            
            //corner case so it will not iterate through an empty shopping list
            while(TakeID < lastTakeID && lastTakeID != null) {
                //convert id to string to match localStorage
                TakeID.toString();
                var newCourse = localStorage.getItem(TakeID);
                TakeID++;
                
                //corner case so it skips a removed course in localstorage
                if(newCourse == null)
                    continue;
                    
                //hides the default empty shopping list text.  This will only trigger if a non-null course is retrieved 
                $("#default_text").hide(); 
                  
                //retrieves course object via stored JSON string
                var course = jQuery.parseJSON(newCourse);
                
                //dynamically generates each item in shopping list using retrieved object
                $("#enrolled_list").append (
                    '<div data-role="collapsible" data-content-theme="c">' + 
                    
                        '<h3>' + course.title + '</h3>' + 
                        
                        '<div id="textbox">' +
                            '<h4 class="alignleft">' + course.department + '</h4>' + 
                            '<h4 class="alignright">' + course.term + '</h4>' + 
                        '</div>' + 
                        '<div style="clear: both;"></div>' + 
                    
                        '<div id="textbox">' + 
                            '<h5 class="alignleft">' + course.faculty + '</h5>' + 
                        '</div>' + 
                        '<div style="clear: both;"></div>' + 
			
                        '<p>' + course.cat_num +  '</p>' + 
                        '<p>' + course.level + '</p>' + 
                        '<p>' + course.gen_ed + '</p>' +
                        '<p>' + course.location + '</p>' +
                        '<p>' + course.meeting_times + '</p>' + 
                        '<p>' + course.description + '</p>' + 
                        
                        //sets each button id to the button name + current TakeID number (there were fewers errors calling
                        //id++ earlier so here that number is decremented.  This way, when clicked these buttons can be
                        //differentiated from each other (e.g. delete101 != delete102)
                        '<button type="submit" data-theme="c" class="remove_button" id="remove' + (TakeID - 1) + '">Remove From Enrolled Courses</button></div>' +

                    '</div>'
                    
                );

            }
            
            

        });
        
        $(function() {
            
            //remove button will response to input
            $(".remove_button").click(function(){
                var removeButton =  $(this).attr('id').replace('remove','');
                localStorage.removeItem(removeButton);
                location.reload();
                //$("#page").buttonMarkup;
            });
            
            $("#default_button").click(function(){
                localStorage.setItem('lastTakeID', 100);
            });
        });
        
    </script>

    <!-- buttons for navigation to the search menu and the shopping list -->
    <a href="/sidebar" data-icon="gear" data-transition="slide" data-direction="reverse">Search Courses</a>
    <a href="/shopping" data-icon="star"  data-transition="pop"> Courses I'm Shopping</a>
    </div><!-- /header -->

    <div data-role="content">	

        <div id="enrolled_list" data-role="collapsible">
            <h3>Courses I'm Taking</h3>
            
            <div id="default_text">
                <h3 align="center">You Are Not Currently Enrolled in Any Classes!</h3>
                <a href="/sidebar" id="default_button"data-role="button" data-theme="b" data-transition="slide" data-direction="reverse">Try a Search?</a>
            </div>	
            
        </div>
        

        <!-- our live tile idea simply was way too time consuming for our level of experience/schedule.  We offer this somewhat sexier alternative -->
        <div data-role="collapsible" data-collapsed="false">
            <h3>Live Rob Tiles!</h3>
            
            <fieldset class="ui-grid-a">
                <div class="ui-block-a">
                    <img class="rob_tile" alt="" src=""/>
                </div>
                <div class="ui-block-b">
                    <img alt="" class="rob_tile" src=""/>
                </div>
                <div class="ui-block-a">
                    <img class="rob_tile" alt="" src=""/>
                </div>
                <div class="ui-block-b">
                    <img alt="" class="rob_tile" src=""/>
                </div>
                <div class="ui-block-a">
                    <img class="rob_tile" alt="" src=""/>
                </div>
                <div class="ui-block-b">
                    <img alt="" class="rob_tile" src=""/>
                </div>
                <div class="ui-block-a">
                    <img class="rob_tile" alt="" src=""/>
                </div>
                <div class="ui-block-b">
                    <img alt="" class="rob_tile" src=""/>
                </div>
                <div class="ui-block-a">
                    <img class="rob_tile" alt="" src=""/>
                </div>
                <div class="ui-block-b">
                    <img alt="" class="rob_tile" src=""/>
                </div>
            </fieldset>
        
	</div><!-- /content -->

</div><!-- /page -->
