<!-- *********************************************************
 *
 * Result/index.php
 * 
 * View For Detailed Course Info
 *
 * Buffalo Hird
 * Computer Science 164
 * Project0
 *
 *
 ********************************************************* -->
    <script>
        //adds what is currently default text to shopping list when user adds class to shoppng list.  alerts success if these values are defined in localstorage
        $(document).ready(function(){
        
            //localStorage.clear();
            $("#shop").click(function(){
                
                // grab the next available ID from storage
                var lastID = localStorage.getItem('lastID');
 
                // if the local storage comes up empty, reset count to zero
                if(lastID == null) {
                    localStorage.setItem('lastID', 0);
                    var lastID = 0;
                }
            
 
            
                //create a ID number for the course 
                var courseID = lastID;
            
            
                //creates an object to storage all course details
            
                var newCourse = {};
                    newCourse.title = $('#title').text();
                    newCourse.term = $('#term').text();
                    newCourse.department = $('#department').text();
                    newCourse.cat_num = $('#cat_num').text();
                    newCourse.level = $('#level').text();
                    newCourse.gen_ed = $('#gen_ed').text();
                    newCourse.location = $('#location').text();
                    newCourse.meeting_times = $('#meeting_times').text();
                    newCourse.faculty = $('#faculty').text();
                    newCourse.description = $('#description').text();
                    newCourse.notes = $('#notes').text();
             
              
                //list starts at 0
                ID = 0;
                var newBool = true;
                //iterates through list (of length LastID) to check if course already exists
                while(ID < lastID) {
                    //convert id to string to match localStorage
                    ID = ID.toString();
                
                    var checkCourse = localStorage.getItem(ID);
                    //prompts if the attempted addition matches a class already on the list
                    if(checkCourse == JSON.stringify(newCourse)) {
                        $("#shop").text('You Are Shopping This!').buttonMarkup();  
                        //creates a bool conditional which will not allow the course to be added to the shopping list
                        newBool = false;  
                        break;
                    }
                    ID++;
                } 
             
                //occurs if there is no error flagged by the identical course check
                if(newBool == true) {   
                    //turn course data into JSON string for storage
                    localStorage.setItem(courseID, JSON.stringify(newCourse));
            
                    //increments the course ID and stores it locally
                    courseID++;
                    //item removed then added to avoid compatibility errors
                    localStorage.removeItem('lastID');
                    localStorage.setItem('lastID', courseID);
                
                    $("#shop").text('You Are Shopping This!').buttonMarkup(); 
                }
            
            


            

            });
        
                //take button will response to input
                //we settle upon 100 being the baseline numerical list for enrolled courses because it would be insane to
                //imagine someone shopping 100 courses (those individuals will drop out and invent facebook).  For our 
                //limited experience and understanding, this is a functional and acceptable approach.
                $("#take").click(function(){
                
                
                    // grab the next available TakeID from storage
                    var lastTakeID = localStorage.getItem('lastTakeID');
 
                    // if the local storage comes up empty, reset count to 100 (take's zero)
                    if(lastTakeID == null) {
                        localStorage.setItem('lastTakeID', 100);
                        var lastTakeID = 100;
                    }
                
                    //creates an ID Number for the course
                    var courseTakeID = lastTakeID;
                
                //creates an object to storage all course details
            
                var newCourse = {};
                    newCourse.title = $('#title').text();
                    newCourse.term = $('#term').text();
                    newCourse.department = $('#department').text();
                    newCourse.cat_num = $('#cat_num').text();
                    newCourse.level = $('#level').text();
                    newCourse.gen_ed = $('#gen_ed').text();
                    newCourse.location = $('#location').text();
                    newCourse.meeting_times = $('#meeting_times').text();
                    newCourse.faculty = $('#faculty').text();
                    newCourse.description = $('#description').text();
                    newCourse.notes = $('#notes').text();
                
                
                //we start a counter at 100 and set default (bool as true) to proceed with enrolling in the course
                takeID = 100;
                var takeBool = true;
                
                //iterate through all courses in taken list
                while(takeID < lastTakeID) {
                    
                    takeID = takeID.toString();
                    var checkTake = localStorage.getItem(takeID);
                    // if this check ever matches an enrolled course in localStorage, the adding process will abort, as flagged by the boolean
                    if(checkTake == JSON.stringify(newCourse)) {
                        //shows user course is already being taken
                        $("#take").text('You are Taking This').buttonMarkup();
                        takeBool = false;
                        break;
                    }
                    takeID++;
                }
                
                //all proceeding without error
                if(takeBool == true) {
                    
                    localStorage.setItem(courseTakeID, JSON.stringify(newCourse));
    
                    //variable used to increment the final TakeID
                    courseTakeID++;
                    
                    //removed and readded for compatibility reasons
                    localStorage.removeItem('lastTakeID');
                    localStorage.setItem('lastTakeID', courseTakeID);
                    
                    //shows user that course is being taken
                    $("#take").text('You are Taking This').buttonMarkup();
                }
                
            });
        });
        
    </script>

    <!-- menu bar button to return to search -->
    <a href="/sidebar" data-icon="gear" class="ui-btn-left"  data-transition="slide" data-direction="reverse" >Search</a>

    <!-- //TODO button to add to shopping list -->
	<a id="shop">Shop This Course!</a>
    
	</div><!-- /header -->

	<div data-role="content">	

        <!-- some css finagling to have text display on both left and right margins -->
		<div id="textbox">
 			<h2 id="title" class="alignleft"><?= $course->title ?></h2>
  			<h3 id="term" class="alignright">Spring, 2012</h3>
		</div>
		
		<div style="clear: both;"></div>
  		 
		
		<div data-role="collapsible">
   			<h3>Overview</h3>
   			<p id="department">Department: <?= $course->dept ?></p>
   			<p id="cat_num">Catalog Number: <?= $course->cat_num ?></p>
   			<p id="level">Course Level: <?= (empty($course->course_level) ? "This course does not list a course level." : $course->course_level) ?></p>
   			<p id="location">General Education: <?= (empty($course->gen_ed) ? "This course does not list any Gen Ed requirements." : $course->gen_ed) ?></p>
            <p id="gen_ed">Location: <?= (empty($course->location) ? "This course does not list any locations." : $course->location) ?></p>
   			<p id="meeting_times">Meeting Times: <?= ((empty($course->days) || empty($course->times)) ? "This course does not list any meeting times." : $course->times . " on " . $course->days) ?></p>
   			
		</div>
		
		<div data-role="collapsible">
   			<h3>Faculty</h3>
   			<p id="faculty">Faculty: <?= ((empty($course->prefix) ? "" : $course->prefix . " ") . 
   							$course->first . 
   							(empty($course->middle) ? " " : " " . $course->middle . " ") .
   							$course->last .
   							(empty($course->suffix) ? "" : " " . $course->suffix))
   							
   						?></p>
		</div>
		
		<div data-role="collapsible">
   			<h3>Description</h3>
   			<p id="description">Description: <?= (empty($course->description) ? "This course does not have a description." : $course->description) ?></p>
   			<p id="notes">Notes: <?= (empty($course->notes) ? "This course does not have any notes ya bastid." : $course->notes) ?></p>
		</div>
        
        <a id="take"data-role="button" data-theme="b">Take this Course</a>

		
	</div><!-- /content -->

</div><!-- /page -->
