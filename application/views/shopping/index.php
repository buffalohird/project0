<!-- *********************************************************
 *
 * Shopping/index.php
 * 
 * View For Shopping List
 *
 * Buffalo Hird
 * Computer Science 164
 * Project0
 *
 *
 ********************************************************* -->
    <script>
        
        //sets the first course information based on localStorage info
        $(function() {
            
            //loads number of courses from storage so that each can be added to shopping list
            var lastID = localStorage.getItem('lastID');
            //list starts at 0
            ID = 0;
            
            //corner case so it will not iterate through an empty shopping list
            while(ID < lastID && lastID != null) {
                //convert id to string to match localStorage
                ID.toString();
                //TODO convert JSON string into each field of course and add to shopping list page
                //var newCourse = jQuery.parseJSON(localStorage.getItem('ID'));
                var newCourse = localStorage.getItem(ID);
                ID++;
                
                //corner case so it skips a removed course in localstorage
                if(newCourse == null)
                    continue;
                    
                //hides the default empty shopping list text.  This will only trigger if a non-null course is retrieved 
                $("#default_text").hide(); 
                  
                //retrieves course object via stored JSON string
                var course = jQuery.parseJSON(newCourse);
                
                //dynamically generates each item in shopping list using retrieved object
                $("#shopping_list").append (
                    '<div data-role="collapsible" data-content-theme="c">' + 
                    
                        '<h3>' + course.title + '</h3>' + 
                        
                        '<div id="textbox">' +
                            '<h4 class="alignleft">' + course.department + '</h4>' + 
                            '<h4 class="alignright">' + course.term + '</h4>' + 
                        '</div>' + 
                        '<div style="clear: both;"></div>' + 
                    
                        '<div id="textbox">' + 
                            '<h5 class="alignleft">' + course.faculty + '</h5>' + 
                        '</div>' + 
                        '<div style="clear: both;"></div>' + 
			
                        '<p>' + course.cat_num +  '</p>' + 
                        '<p>' + course.level + '</p>' + 
                        '<p>' + course.gen_ed + '</p>' +
                        '<p>' + course.location + '</p>' +
                        '<p>' + course.meeting_times + '</p>' + 
                        '<p>' + course.description + '</p>' + 
                        
                        '<div class="ui-grid-a">' + 
                            //sets each button id to the button name + current ID number (there were fewers errors calling
                            //id++ earlier so here that number is decremented.  This way, when clicked these buttons can be
                            //differentiated from each other (e.g. delete1 != delete2)
                            '<div class="ui-block-a"><button type="submit" data-theme="c" class="remove_button" id="remove' + (ID - 1) + '">Remove From Shopping List</button></div>' +
                            '<div class="ui-block-b"><button type="submit" data-theme="b" class="take_button" id="take' + (ID - 1) + '">Take Course</button></div>' +	   
                        '</div>' + 
                    '</div>'
                    
                );

            }
            
            

        });
    
        $(function() {
            
            //remove button will response to input
            $(".remove_button").click(function(){
                var removeButton =  $(this).attr('id').replace('remove','');
                localStorage.removeItem(removeButton);
                location.reload();
                //$("#page").buttonMarkup;
                
                
            });
            
            //take button will response to input
            //we settle upon 100 being the baseline numerical list for enrolled courses because it would be insane to
            //imagine someone shopping 100 courses (those individuals will drop out and invent facebook).  For our 
            //limited experience and understanding, this is a functional and acceptable approach.
            $(".take_button").click(function(){
                
                
                // grab the next available TakeID from storage
                var lastTakeID = localStorage.getItem('lastTakeID');
 
                // if the local storage comes up empty, reset count to 100 (take's zero)
                if(lastTakeID == null) {
                    localStorage.setItem('lastTakeID', 100);
                    var lastTakeID = 100;
                }
                
                //creates an ID Number for the course
                var courseTakeID = lastTakeID;
                
                //get the course's ID number from the id and use this to get the course from localStorage
                var takeButton =  $(this).attr('id').replace('take','');
                var takeCourse = localStorage.getItem(takeButton);
                
                //we start a counter at 100 and set default (bool as true) to proceed with enrolling in the course
                takeID = 100;
                var takeBool = true;
                
                //iterate through all courses in taken list
                while(takeID < lastTakeID) {
                    
                    takeID = takeID.toString();
                    var checkTake = localStorage.getItem(takeID);
                    // if this check ever matches an enrolled course in localStorage, the adding process will abort, as flagged by the boolean
                    if(checkTake == takeCourse) {
                        //shows user course is already being taken
                        $(this).text('You are Taking This!').buttonMarkup();
                        takeBool = false;
                        break;
                    }
                    takeID++;
                }
                
                //all proceeding without error
                if(takeBool == true) {
                    
                    localStorage.setItem(takeID, takeCourse);
    
                    //variable used to increment the final TakeID
                    courseTakeID++;
                    
                    //removed and readded for compatibility reasons
                    localStorage.removeItem('lastTakeID');
                    localStorage.setItem('lastTakeID', courseTakeID);
                    
                    //shows user that course is being taken
                    $(this).text('You are Taking This!').buttonMarkup();
                }
                
            });
            
            //chooses this moment, when there will clearly be no items in storage, to reset the ID counter
            $("#default_button").click(function(){
                localStorage.setItem('lastID', 0);
            });
        });
    
    
    </script>
    
    <!-- button shortcut to return to homepage -->
	<a href="/" data-icon="grid" class="ui-btn-left"  data-transition="pop" data-direction="reverse" >Home</a>
	</div><!-- /header -->

	<div data-role="content" id="shopping_list">
        
        <div id="default_text">
            <h3 align="center">You Are Not Currently Shopping Any Classes!</h3>
            <a href="/sidebar" id="default_button"data-role="button" data-theme="b" data-transition="slide" data-direction="reverse">Try a Search?</a>
        </div>	
		
       
		
	</div><!-- /content -->

</div><!-- /page -->
