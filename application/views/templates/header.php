<!-- *********************************************************
 *
 * header.php
 * 
 * Holds required files for each page
 *
 * Buffalo Hird
 * Computer Science 164
 * Project0
 *
 *
 ********************************************************* -->

<!DOCTYPE html>

<!-- loads in required .js and .css files -->
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <title><?php echo htmlspecialchars($title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.css" />
    <!-- custom css file loaded from web to prevent errors -->
    <link rel="stylesheet" href="/css/grid.css" />
	<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
  </head>
  
  <body>
     
    <!-- allows for dynamic titling of pages using the header-->
    <div data-role="page" id="page">

    <div data-role="header" data-theme="b" >
          
          <h1><?php echo htmlspecialchars($title) ?></h1>
          

