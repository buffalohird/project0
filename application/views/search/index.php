<!-- *********************************************************
 *
 * search/index.php
 * 
 * View For Search Results
 *
 * Buffalo Hird
 * Computer Science 164
 * Project0
 *
 *
 ********************************************************* -->


    <!-- menu bar button to return home -->
	<a href="/" data-icon="grid" class="ui-btn-right" >Home</a>
	</div><!-- /header -->

    <!-- Another home button to orient menu //TODO more menu options -->
	<div data-role="content" data-theme="a">	
		<a href="/" data-role="button" data-corners="false" data-inline="true" data-theme="a">CS164 Courses</a>
        
        <form action="/search/search">

        <input type="textfield" value="search for courses"/>
        <button value="Search!"/>
        
		</form>

		</br>
        <!-- search results dynamically generated with php, quick results display include number and title -->
      	<ul data-role="listview" data-theme="a" data-divider-theme="a" class="course_results">
    		<li data-role="list-divider" >Courses</li>
    		
    		<!--This loop dynamically generates our site's search results. When you click on a class, it brings you to a page that displays more information about that class.
    		 We're going to try to add an ID (the class's course number) to each item in the list, and then use that ID to load more information about the class
    		 from our database. We're currently working on the most efficient and CodeIgniter compliant way to go about this. -->
    		 
    		<?php foreach($query as $row): ?>
    		<li id="search_result"><a href= "result">Category Number: <?=  $row->cat_num?><br />Title: <?= $row->title?><br />Course Level: <?= $row->course_level ?> <br />Department: <?= $row->course_group?></a></li>
    		<?php endforeach; ?>
		</ul>
		

    	
		
	</div><!-- /content -->

</div><!-- /page -->
