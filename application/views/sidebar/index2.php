<!-- *********************************************************
 *
 * sidebar/index2.php
 * 
 * View For Accessing The Menu / Searching
 *
 * Buffalo Hird
 * Computer Science 164
 * Project0
 *
 *
 ********************************************************* -->
    <script>
    	
        
        // on load, sets this event handler to alert hi //ajax call to /sidebar/search when 'enter' is pressed in the search field
        $(function() {
        	//document.getElementById('show_recents').reload();
        
            $("#submit").bind("keypress", function(e) {

                var c = e.which ? e.which : e.keyCode;
                if (c == 13) 
                {

                    var search_val = $("#search").val();
                    var gen_ed = $("#gen_ed").val();
                    var days = $("#days").val();
                    var times = $("#times").val();
            
                    //remove all previous results
                    $(".course_search_result").remove();
          
                    if(search_val == "")
                    {
                        return false;
                    }

                    var pass = 	{'key' : search_val,
                                'gen_ed': gen_ed,
                                'days': days,
                                'times' : times,
                                }

                    $.post('/sidebar/search_courses', pass, function(data) 
                    {
                        if(data == "empty")
                            // this html change doesn't work, "empty" is written as a plain text item where it should be.  
                            // it might have to do with how the $message variable works in php
                            $('#default_result').text("Your search returned no results");
                            //$('#show_courses').hide();
                        else
                        {
                            //$('#show_courses').show();
                            $('#show_courses').append(data)
                            .listview('refresh');
                            //hides no course result notification when courses are returned
                            $('#default_result').hide();
                        }
                    });
		    
		    
                    //prevents HTML from activating get request
                    e.preventDefault();
                    return false;

                }

            });
        });
            
        $(function() {

            var pass = 	{'status' : 'good',
                        }
				
            console.log("testing");
            $.post('/sidebar/show_recents', pass, function(data) 
            {
                if(data == "empty")
                    // this html change doesn't work, "empty" is written as a plain text item where it should be.  
                    // it might have to do with how the $message variable works in php
                    $('#default_result').text("Your search returned no results");
                    //$('#show_courses').hide();
                else
                {
                    $(".course_recent_result").remove();
                    //$('#show_courses').show();
                    $('#show_recents').append(data).
                    listview('refresh');
                        
                }
            });
		    
		    
            //prevents HTML from activating get request
            return false;

        });



    </script>

    <!-- loads in the form used to get search results -->

    <!-- menu bar button to return home -->
	<a href="/" data-icon="grid" class="ui-btn-right" >Home</a>
	</div><!-- /header -->

    <!-- Another home button to orient menu //TODO more menu options -->
	<div data-role="content" data-theme="a">	
		<a href="/" data-role="button" data-corners="false" data-inline="true" data-theme="a">CS164 Courses</a>
        
        <form id="submit">
            
            <input type="search" id="search" placeholder="search a course's title, department, etc." />
            <!-- <input type="radio" id="gen_ed">General Education</input> -->
            <!-- <button id="sidebar_button" name="Search!"/> -->
            
            <select id="gen_ed" data-theme="a">
                <option value="null">Specify A General Education Criterion</option>
                <option value="United States in the World">United States in the World (US/W)</option>
                <option value="Societies of the World">Societies of the World (SW)</option>
                <option value="Science of the Physical Universe">Science of the Physical Universe (SPU)</option>
                <option value="Science of Living Systems">Science of Living Systems (SLS)</option>
                <option value="Ethical Reasoning">Ethical Reasoning (ER)</option>
                <option value="Empirical and Mathematical Reasoning">Empirical and Mathematical Reasoning (EMR)</option>
                <option value="Culture and Belief">Culture and Belief (CB)</option>
                <option value="Aesthetic and Interpretive Understanding"> Aesthetic and Interpretive Understanding (AI)</option>
            </select>
            
            <select id="days" data-theme="a">
                <option value="null">Specify Days You're Availible</option>
                <option value="Monday, Wednesday, Friday">Monday, Wednesday, Friday</option>
                <option value="Tuesday, Thursday">Tuesday, Thursday</option>
                <option value="Monday, Wednesday">Monday, Wednesday</option>
                <option value="Monday">Monday</option>
                <option value="Tuesday">Tuesday</option>
                <option value="Wednesday">Wednesday</option>
                <option value="Thursday">Thuesday</option>
                <option value="Friday">Friday</option>
            </select>
            
            <select id="times" data-theme="a">
                <option value="null">Specify Times You're Availible</option>
                <option value="1000">at 10:00</option>
                <option value="1100">at 11:00</option>
                <option value="1200">at noon</option>
                <option value="1300">at 1:00</option>
                <option value="1400">at 2:00</option>
                <option value="1500">at 3:00</option>
                <option value="1600">at 4:00</option>
                <option value="1700">at 5:00</option>
                <option value="1800">at 6:00</option>
                <option value="1900">at 7:00</option>
                <option value="2000">at 8:00</option>
            </select>

        </form>
        
		</br>
		

		
        <!-- displays when no searches have yet been made -->
      	<ul data-role="listview" data-theme="a" data-divider-theme="a" class="course_results" id="show_courses">
    		<li data-role="list-divider" >Courses</li>
    		<li id="default_result"><h2><i><?= $message ?></i></h2></li>
		</ul>
        
        </br></br>
        
        <ul data-role="listview" data-theme="a" data-divider-theme="a" class="course_recents" id="show_recents">
            <li data-role="list-divider" >Recently Viewed</li>
        </ul> 
		
	</div><!-- /content -->

</div><!-- /page -->
