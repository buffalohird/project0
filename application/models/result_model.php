<?php

	/***********************************************************
	 *
	 * course_lookup_model.php
	 * 
	 * Model for looking up coudrses.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/

	// extend the CI model framework
	class Result_Model extends CI_Model 
	{

		// construct the parent
		public function __construct() 
		{
		    parent::__construct();
		}
		
		/*
		 * Returns an array of courses to load into the user's Search field
		 *
		 * We track the users' search histories in the db, can allow for seeing who's searching what later on
		 *
		 */
		
		// gather everything about a course
		public function get_info($cat_num)
		{
			// prepare to add the course
			$data = array('recent.cat_num' => $cat_num);

			// add the course to a list of recently viewed courses. If it fails, it was already there
			if(!$this->db->insert('recent', $data))
				{ }
			
			// join our relational tables where the course's cat_num equals our cat_num
			$this->db->select('*');
			$this->db->from('course_info');
			$this->db->join('course_faculty', 'course_faculty.cat_num = course_info.cat_num');
			$this->db->join('faculty_info', 'course_faculty.instructor_id = faculty_info.id');
			$this->db->where('course_info.cat_num', $cat_num);
			
			// returns an array of courses
			return $this->db->get()->row();
		}
    
	}
?>
