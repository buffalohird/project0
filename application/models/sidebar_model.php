<?php

	/***********************************************************
	 *
	 * sidebar_model.php
	 * 
	 * Model for looking up courses.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/

	// extend the model, load the Sidebar_model class
	class Sidebar_Model extends CI_Model 
	{
		// contruct a parent
		public function __construct() 
		{
		    parent::__construct();
		}
	
		/*
		 * Returns an array of courses to load into the user's Search field
		 */
		
		// get the user's recently viewed courses from the db
		public function get_recents()
		{
			// join our relational tables and limit recent courses to the last 5
			$this->db->join('course_info', 'course_info.cat_num = recent.cat_num');
			$this->db->order_by('time DESC');
			$this->db->limit(5);
			
			// return an array of recently viewed course objects
			return $this->db->get('recent')->result();
		}
		
		// get courses for when a user enters a search
		public function get_courses($key, $gen_ed, $days, $times)
		{
			// explode the search (for names)
			$exploded = explode(" ", $key);
			$word_num = count($exploded);
			$key_arr = array();
			
			// let's not go crazy here
			if($word_num > 8 || empty($key))
				return "failure";
			
			// have every field accesible for the user's search
			$this->db->select('*');	
			
			// join our relational tables
			$this->db->from('course_info');
			$this->db->join('course_faculty', 'course_faculty.cat_num = course_info.cat_num');
			$this->db->join('faculty_info', 'course_faculty.instructor_id = faculty_info.id');
			
			// allow a user to search by gen_ed
			/*f($gen_ed != null)
			{
				// search by gen_ed drop down and text search
				$this->db->where('gen_ed', $gen_ed);
				$this->db->like('title', $key);
				//$this->db->or_like('dept', $key);
				//$this->db->or_like('course_level', $key);
				
				// set our limit to 20 courses
				$this->db->limit(20);
			
				// returns an array of course objects
				return $this->db->get()->result();
			}	
			
			// allow a user to search by days
			else if($days != null)
			{
				$this->db->where('days', $days);
				$this->db->like('title', $key);
				
				// set our limit to 20 courses
				$this->db->limit(20);
			
				// returns an array of course objects
				return $this->db->get()->result();
			}
			
			// allow a user to search by days
			else if($times != null)
			{
				$this->db->like('times', $times);
				$this->db->like('title', $key);
				
				// set our limit to 20 courses
				$this->db->limit(20);
			
				// returns an array of course objects
				return $this->db->get()->result();
			}*/
				
			// some possible name combinations
			if($word_num < 6)
			{				
				// the number of words in the user's string
				switch ($word_num) 
				{
					// first and last name
					case 2:
						$two_name_check = array('first' => $exploded[0], 'last' => $exploded[1]);
						$this->db->like($two_name_check);
						break;
					
					// first, middle, last
					case 3:
						$three_name_check = array('first' => $exploded[0], 'middle' => $exploded[1], 'last' => $exploded[2]);
						$this->db->like($three_name_check);
						break;
						
					// prefix, first, middle, last
					case 4: //not many people have suffixes
						$four_name_check = array('prefix' => $exploded[0], 'first' => $exploded[1], 'middle' => $exploded[2], 'last' => $exploded[3]);
						$this->db->like($four_name_check);
						break;
					
					// prefix, first, middle, last, suffix
					case 5:
						$five_name_check = array('prefix' => $exploded[0], 'first' => $exploded[1], 'middle' => $exploded[2], 'last' => $exploded[3], 'suffix' => $exploded[4]);
						$this->db->like($five_name_check);
						break;
				}	
			
			
			
			// other criteria for a general search
			$this->db->or_like('title', $key);
			$this->db->or_like('gen_ed', $key);
			$this->db->or_like('course_level', $key);
			$this->db->or_like('dept', $key);
			$this->db->or_like('course_info.cat_num', $key);
			$this->db->or_like('description', $key);
			$this->db->or_like('notes', $key);
			$this->db->or_like('num_int', $key);
			$this->db->or_like('num_char', $key);
			$this->db->or_like('first', $key);
			$this->db->or_like('last', $key);
			$this->db->or_like('days', $key);
			$this->db->or_like('times', $key);
			
			// set our limit to 20 courses
			$this->db->limit(20);
			
			// returns an array of course objects
			return $this->db->get()->result();
			
			}
		}
	}
?>
