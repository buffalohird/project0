<?php

	/***********************************************************
	 *
	 * courses_lookup_model.php
	 * 
	 * Model for looking up coudrses.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/

	// extend Model and loads Sidebar class
	class Sidebar extends CI_Model {

    /*
     * Returns an array of courses to load into the user's Search field
     */
    
    public function get_courses()
    {
        $sql = "SELECT id, title FROM courses_lookup WHERE cat_num=7010";
        
        $this->db->query($sql);
        
        return $this->db->get()->result();
    }
}

?>
