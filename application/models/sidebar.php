<?php

	/***********************************************************
	 *
	 * sidebar.php
	 * 
	 * Model for looking up coudrses.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/

	class Sidebar extends CI_Model {

    /*
     * Returns an array of courses to load into the user's Search field
     *
     * When model is loaded from controller = $this->load->model('courses_lookup_model', array('course' => 'what_the_user_searched'));
     */
    
    public function get_courses($key)
    {
    //$this->db->select('cat_num', 'title');
    $this->db->like('title', $key);
    
    $this->db->limit(5);
    //$this->db->get('course_lookup');
        
    return $this->db->get('course_info');
    }
}

?>
