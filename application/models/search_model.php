<?php

	/***********************************************************
	 *
	 * search_model.php
	 * 
	 * Model for looking up courses.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/

	// extend the model, load the Search_model class
	class Search_Model extends CI_Model 
	{
		/*
		 * Returns an array of courses to load into the user's Search field
		 */
		
		public function get_courses($key)
		{
			// limits responses to 25, and looks for anything that relates to what the user inputted
			$this->db->like('title', $key);
			$this->db->or_like('notes', $key);
			$this->db->or_like('description', $key);
			$this->db->or_like('gen_ed', $key);
			$this->db->or_like('course_level', $key);
			$this->db->or_like('dept', $key);
			$this->db->limit(50);
				
			// return an array of course objects
			return $this->db->get('course_info')->result();
		}
	}
?>
