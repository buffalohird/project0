<?php

/***********************************************************
 *
 * course_lookup_model.php
 * 
 * Model for looking up coudrses.
 *
 * Ansel Duff
 * Computer Science 164
 * Project0
 *
 *
 ************************************************************/

class Sidebar extends CI_Model {

    /*
     * Returns an array of courses to load into the user's Search field
     *
     * When model is loaded from controller = $this->load->model('courses_lookup_model', array('course' => 'what_the_user_searched'));
     */
    
    public function get_courses()
    {
        $sql = "SELECT id, title FROM courses_lookup WHERE cat_num=7010";
        
        $this->db->query($sql);
        
        return $this->db->get()->result();
    }
}

?>
