<?php

	/***********************************************************
	 *
	 * shopping_model.php
	 * 
	 * Model for looking up courses.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/

	// extend the model, load the Search_model class
	class Sidebar_Model extends CI_Model 
	{
		/*
		 * Returns an array of courses to load into the user's Search field
		 */
		
		public function shop_courses($cat_num)
		{
			// limits responses to 25, and looks for anything that relates to what the user inputted
			this->db->get_where(cat_num, $cat_num);
				
			// returns a single course object
			return $this->db->get('course_info')->result();
		}
	}
?>
