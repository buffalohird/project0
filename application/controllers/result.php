<?php
	
	/***********************************************************
	 *
	 * result.php
	 * 
	 * Controller for results page.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/
	
	// cary the controller class
	class Result extends CI_Controller 
	{
		// construct from parent, load the model
		public function __construct()
		{
		    parent::__construct();
			$this->load->model('Result_Model');
    	}
		
		// when the page loads
		public function index()
		{
		
			// from url
			$cat_num = $this->input->get('cat_num');
			
			// prepare data for the views
			$data['title'] = 'More Info!';
			$data['course'] = $this->Result_Model->get_info($cat_num);
			
			// load the views
			$this->load->view('templates/header', $data);
			$this->load->view('result/index', $data);
			$this->load->view('templates/footer');
		}
	}
?>
