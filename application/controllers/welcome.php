<?php
	
	/***********************************************************
	 *
	 * welcome.php
	 * 
	 * Controller for welcome page.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/
	 
	// again, carry the controller class
	class Welcome extends CI_Controller
	{
		public function index()
		{
			// load views
			$this->load->view('templates/header', array('title' => 'CS164 Courses'));
			$this->load->view('welcome/index');
			$this->load->view('templates/footer');
		}
	}
?>
