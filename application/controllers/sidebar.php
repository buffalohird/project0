<?php
	
	/***********************************************************
	 *
	 * sidebar.php
	 * 
	 * Controller for sidebar
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/
	
	// extend Sidebar class for Controller
	class Sidebar extends CI_Controller 
	{
		// construct from parent, load the model
		public function __construct()
		{
		    parent::__construct();
			$this->load->model('Sidebar_Model');
    	}

		// when the page loads
		public function index()
		{
			// initiate the user's session
			session_start();
			
			// allows for dynamic titles and messages
			$data['title'] = 'Search for Courses';
			$data['message'] = 'You currently have no courses to view.';
	
			// load the views
			$this->load->view('templates/header', $data);
			$this->load->view('sidebar/index2');
			$this->load->view('templates/footer');
		}
		
		// search for courses
		public function search_courses()
		{
		    
		    // get parameters from post
		    $key = $this->input->post('key');
		    $gen_ed = $this->input->post('gen_ed');
		    $days = $this->input->post('days');
		    $times = $this->input->post('times');

		    // search courses (throw to the model)
		    $data['query'] = $this->Sidebar_Model->get_courses($key, $gen_ed, $days, $times);
		    
		    // something went wrong
		    if($data['query'] == "failure")
		    	exit;
		    
		    // something didn't go wrong
		    $data['results_query'] = $this->Sidebar_Model->get_recents();
		    
		    // allows for dynamic titles
			$data['title'] = 'Results';
			$data['message'] = 'Your results are ...';
	
			// inject the results into the existing view
		    $this->load->view('sidebar/index3', $data);
   
    	}
    	
    	// show the 5 most recent courses the user viewed
    	public function show_recents ()
    	{
    		$recents['result'] = $this->Sidebar_Model->get_recents();

    		$this->load->view('sidebar/index4', $recents);
    	}
    	
	}
?>
