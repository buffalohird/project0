<?php
	
	/***********************************************************
	 *
	 * search.php
	 * 
	 * Controller for Search page.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/
	 
	// extend Search class for Controller
	class Search extends CI_Controller 
	{
		public function __construct()
		{
		    parent::__construct();
			$this->load->model('search_model');
    	}

		public function search() 
		{
		    // get parameters from post
		    $key = $this->input->post;

		    // search courses
		    $data['query'] = $this->search_model->get_courses($key);
    	}
		
		public function index()
		{
			// allows for dynamic titles
			$data['title'] = 'Result';
	
			// load the view	
			$this->load->view('templates/header', $data);
			$this->load->view('search/index', $data);
			$this->load->view('templates/footer');
		}
	}
?>
