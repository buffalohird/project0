<?php
	
	/***********************************************************
	 *
	 * shopping.php
	 * 
	 * Controller for shopping page.
	 *
	 * Ansel Duff
	 * Computer Science 164
	 * Project0
	 *
	 *
	 ************************************************************/
	 
	// once again the controller class
	class Shopping extends CI_Controller
	{

		public function index()
		{
			// load the views
			$this->load->view('templates/header', array('title' => 'Shopping List'));
			$this->load->view('shopping/index');
			$this->load->view('templates/footer');
		}
	}
?>
